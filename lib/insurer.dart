library insurer;

import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';

import 'ui/processing_page.dart';

class PluginInsurer {
  final StreamController<int> _streamController = StreamController.broadcast();

  void runPlugin(context, Function callBack, String siteUrl, String trigger, {String pluginName = 'Verzekeraar'}) async {
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ProcessingPage(siteUrl),
      ),
    ).then(
      (value) =>
          // fire the callback function to send the data back to the app
          callBack(
        'insurer',
        jsonEncode({'insurer.wishlist': jsonEncode(jsonDecode(value))}), // only one attribute for now, so we'll keep it simple
        context,
      ),
    );
  }

  /// Gets extracted data
  Future<Map<String, dynamic>> getExtractedData(String jsonData) async {
    _streamController.add(50);
    Map<String, dynamic>? attributes = jsonDecode(jsonData);
    _streamController.add(100);
    return Future.value(attributes);
  }

  Stream<int> getProgress() {
    return _streamController.stream;
  }
}
