import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ProcessingPage extends StatefulWidget {
  final String downloadUrl;

  ProcessingPage(this.downloadUrl);

  @override
  _ProcessingPageState createState() => _ProcessingPageState();
}

class _ProcessingPageState extends State<ProcessingPage> {
  @override
  void initState() {
    super.initState();

    process();
  }

  void process() async {
    // download the whishlist
    var response = await http.get(Uri.parse(widget.downloadUrl));

    if (response.statusCode == 200) {
      // send back the response body while closing this page
      Navigator.pop(context, response.body);
    } else {
      // some error here!
    }
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    var topPadding = MediaQuery.of(context).padding.top;

    //TODO: Moght be nice to show a waiting screen
    return Container(
      child: Text('Ophalen...'),
    );
  }
}
